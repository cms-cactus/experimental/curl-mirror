FROM msoap/shell2http

RUN apk add --no-cache curl

# proxy socks5h://rapture.cern.ch:55555
# example target http://l1ts-central.cms:3333

# proxies the command to the desired URL
# endpoint /metrics

#/metrics 
EXPOSE 9100 19000 9945 4002 9966 10080 3333 4500 5500 7000

# 9100 / node exporter
# 19000
# 9945
# 4002
# 9966
# 80 / l1ce & l1page
# 3333 / sc
# 4500 / sc
# 5500 / sc
# 7000 / ts

COPY SetupProxies.sh SetupProxies.sh

ENTRYPOINT source SetupProxies.sh