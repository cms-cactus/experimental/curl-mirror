#! /bin/sh

# 9100 /metrics 
# 19000  /rcms/gui/servlet/RunGroupChooserServlet/
# 9945  /urn:xdaq-application:lid=16
# 4002 / 
# 9966 / 
# 80 / l1ce & l1page
# 3333 /urn:xdaq-application:lid=13 sc
# 4500 /urn:xdaq-application:lid=13 sc
# 5500 /urn:xdaq-application:lid=13 sc
# 7000 / ts
# 8097 / scouting

echo Starting Proxies
echo HTTP PROXY ${HTTP_PROXY}
echo DESTINATION URL ${DESTINATION_URL}

/app/shell2http -cgi -show-errors -port 9100 /metrics \
  "curl --connect-timeout 5 --max-time 5 -k --fail --proxy ${HTTP_PROXY} -L ${DESTINATION_URL}:9100/metrics ||\
  { echo \"Status: 500\"; echo ; echo \"Something bad happened.\"; }" &

/app/shell2http -cgi -show-errors -port 19000 /rcms/gui/servlet/RunGroupChooserServlet/ \
  "curl --connect-timeout 5 --max-time 5 -k --fail --proxy ${HTTP_PROXY} -L ${DESTINATION_URL}:19000/rcms/gui/servlet/RunGroupChooserServlet/ ||\
  { echo \"Status: 500\"; echo ; echo \"Something bad happened.\"; }" &

# 9945 is used by xaas which also needs a get request. Taking with -form the get parameters and redirecting them with curl

/app/shell2http -form -cgi -show-errors -port 9945 /urn:xdaq-application:lid=16 \
  "curl --connect-timeout 5 --max-time 5 -k --fail --proxy ${HTTP_PROXY} \
  -L ${DESTINATION_URL}:9945/urn:xdaq-application:lid=16 ||\
  { echo \"Status: 500\"; echo ; echo \"Something bad happened.\"; }" \
  /urn:xdaq-application:lid=16/retrieveCollection \
  "curl -G --connect-timeout 5 --max-time 5 -k --fail --proxy ${HTTP_PROXY} \
  \$(for v in \$(env | grep -E \"\bv_\") ; do printf \"-d \"\${v:2}\" \" ; done) \
  -L ${DESTINATION_URL}:9945/urn:xdaq-application:lid=16/retrieveCollection ||\
  { echo \"Status: 500\"; echo ; echo \"Something bad happened.\"; } " &

/app/shell2http -cgi -show-errors -port 4002 / \
  "curl --connect-timeout 5 --max-time 5 -k --fail --proxy ${HTTP_PROXY} -L ${DESTINATION_URL}:4002 ||\
  { echo \"Status: 500\"; echo ; echo \"Something bad happened.\"; }" &

/app/shell2http -cgi -show-errors -port 9966 / \
  "curl --connect-timeout 5 --max-time 5 -k --fail --proxy ${HTTP_PROXY} -L ${DESTINATION_URL}:9966 ||\
  { echo \"Status: 500\"; echo ; echo \"Something bad happened.\"; }" &

/app/shell2http -cgi -show-errors -port 10080 / \
  "curl --connect-timeout 5 --max-time 5 -k --fail --proxy ${HTTP_PROXY} -L ${DESTINATION_URL}:80 ||\
  { echo \"Status: 500\"; echo ; echo \"Something bad happened.\"; }" &

/app/shell2http -cgi -show-errors -port 3333 /urn:xdaq-application:lid=13 \
  "curl --connect-timeout 5 --max-time 5 -k --fail --proxy ${HTTP_PROXY} -L ${DESTINATION_URL}:3333/urn:xdaq-application:lid=13 ||\
  { echo \"Status: 500\"; echo ; echo \"Something bad happened.\"; }" &

/app/shell2http -cgi -show-errors -port 4500 /urn:xdaq-application:lid=13 \
  "curl --connect-timeout 5 --max-time 5 -k --fail --proxy ${HTTP_PROXY} -L ${DESTINATION_URL}:4500/urn:xdaq-application:lid=13 ||\
  { echo \"Status: 500\"; echo ; echo \"Something bad happened.\"; }" &

/app/shell2http -cgi -show-errors -port 5500 /urn:xdaq-application:lid=13 \
  "curl --connect-timeout 5 --max-time 5 -k --fail --proxy ${HTTP_PROXY} -L ${DESTINATION_URL}:5500/urn:xdaq-application:lid=13 ||\
  { echo \"Status: 500\"; echo ; echo \"Something bad happened.\"; }" &

/app/shell2http -cgi -show-errors -port 7000 / \
  "curl --connect-timeout 5 --max-time 5 -k --fail --proxy ${HTTP_PROXY} -L ${DESTINATION_URL}:7000 ||\
  { echo \"Status: 500\"; echo ; echo \"Something bad happened.\"; }" &

/app/shell2http -cgi -show-errors -port 8097 / \
  "curl --connect-timeout 5 --max-time 5 -k --fail --proxy ${HTTP_PROXY} -L ${DESTINATION_URL}:8097 ||\
  { echo \"Status: 500\"; echo ; echo \"Something bad happened.\"; }" 

# l1page.cms
# l1page-test.cms
# l1ce.cms
# l1ce-test.cms
# l1ts-central.cms
# l1ts-ugt.cms
# l1ts-calol1.cms
# l1ts-calol2.cms
# l1ts-ugmt.cms
# l1ts-bmtf.cms
# l1ts-omtf.cms
# l1ts-emtf.cms
# l1ts-twinmux.cms
# l1ts-cppf.cms
# cmsrc-trigger.cms
# l1ts-xaas.cms

# continue by opening 10 shell2bash session, each one on a specific port
# 9100 should be /metrics, not just /
# sc ports should be /urn:xdaq-application:lid=13